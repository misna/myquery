// mongodb change id (string) to ObjectId
db.participants.find({ _id: { $type: 2 } }).forEach(function (d) {
	var id = ObjectId(d._id);
	var oldId = d._id;
	d._id = id;
	db.participants.insert(d);
	db.participants.remove({ _id: oldId });
});
